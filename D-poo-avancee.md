<img src="images/readme/header-small.jpg" >

# D. POO avancée <!-- omit in toc -->

## Sommaire <!-- omit in toc -->
- [D.1. _Composition :_ La classe PizzaThumbnail](#d1-composition-la-classe-pizzathumbnail)
- [D.2. _Composition :_ La classe PizzaList](#d2-composition-la-classe-pizzalist)
- [D.3. _Propriétés et méthodes statiques :_ La classe Router](#d3-propriétés-et-méthodes-statiques-la-classe-router)
	- [D.3.1 Rappels de syntaxe](#d31-rappels-de-syntaxe)
	- [D.3.2. La classe `Router`](#d32-la-classe-router)
- [D.4. _Private, Setter & Getter :_ La propriété `pizzaList.pizzas`](#d4-private-setter-getter-la-propriété-pizzalistpizzas)
	- [D.4.1. Rappels propriétés privées](#d41-rappels-propriétés-privées)
	- [D.4.2. Rappels getters/setters](#d42-rappels-getterssetters)
	- [D.4.3. Mise en oeuvre](#d43-mise-en-oeuvre)

## D.1. _Composition :_ La classe PizzaThumbnail

_**L'objectif de cet exercice est d'utiliser une classe à l'intérieur d'une autre. On va se servir de la classe `Img` développée précédemment à l'intérieur d'un nouveau composant : `PizzaThumbnail`.**_

1. **Modifiez le code de la méthode `render()`** de la classe `Component` pour lui permettre de recevoir dans le paramètre `children` :
	- soit une **chaîne de caractères** (comme c'est déjà le cas actuellement)
	- soit un **tableau de chaînes de caractères.** <br>Par exemple : si `tag` vaut `"div"` et que `children` vaut `[ "youpi", "ça", "marche" ]` alors `render()` retournera la chaîne `"<div>youpiçamarche</div>"`.

	> _**NB :** Pour ne pas alourdir trop le code de la méthode `render()` et pour avoir un code plus lisible, passez le code de rendu des enfants, dans une méthode `renderChildren()`._

	> _**NB2 :** Pour tester si `children` est un tableau (classe `Array`), vous pouvez utiliser l'opérateur `instanceof` cf. https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Op%C3%A9rateurs/instanceof_

	Testez votre classe avec le code suivant :
	```js
	const title = new Component( 'h1', null, ['La', ' ', 'carte'] );
	document.querySelector('.pageTitle').innerHTML = title.render();
	```

2. **Adaptez la méthode `render()`** pour permettre de passer dans `children` non seulement des chaînes de caractères comme c'est déjà le cas mais aussi **d'autres `Component`**, comme ceci :
	```js
	const c = new Component(
		'article',
		{name:'class', value:'pizzaThumbnail'},
		[
			new Img('https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'),
			'Regina'
		]
	);
	document.querySelector( '.pageContent' ).innerHTML = c.render();
	```
	Pour cela, la méthode `renderChildren()` devra maintenant tester le type de chaque enfant :
	- si cet enfant est lui-même une instance de Component, on fait alors appel à la méthode `render()` du `Component` enfant (petit indice : ça ressemble quand même beaucoup au concept de "récursivité"...)
	- si l'enfant est une chaîne de caractères, alors la chaîne est ajoutée telle qu'elle, comme auparavant

	> _**NB :** Pour tester si un enfant est de la classe `Component`, vous pouvez là aussi utiliser l'opérateur [`instanceof`](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Op%C3%A9rateurs/instanceof)_

	Si votre code fonctionne correctement, vous devez avoir le rendu suivant :

	<img src="images/readme/pizzaland-03.png" >

3. **Créez une classe `PizzaThumbnail`** (dans `src/components/PizzaThumbnail.js`) qui hérite de la classe `Component` et qui utilise **obligatoirement** la classe **`Img`** pour rendre son code HTML.

	Avec le code JS suivant :
	```js
	const pizzaThumbnail = new PizzaThumbnail(data[0]);
	document.querySelector( '.pageContent' ).innerHTML = pizzaThumbnail.render();
	```
	... le code HTML généré sera :
	```html
	<article class="pizzaThumbnail">
		<a href="https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300">
			<img src="https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300" />
			<section>
				<h4>Regina</h4>
				<ul>
					<li>Prix petit format : 6.50 €</li>
					<li>Prix grand format : 9.95 €</li>
				</ul>
			</section>
		</a>
	</article>
	```
	... et le rendu obtenu dans la page sera celui-ci :

	<img src="images/readme/pizzaland-04.png" >

## D.2. _Composition :_ La classe PizzaList

_**Après un exemple simple de composition, attaquons nous à un cas plus complexe : celui de la `PizzaList`.**_

Cette classe va nous permettre d'afficher plusieurs vignettes côte à côte grâce au composant `PizzaThumbnail` créé précédemment.

**Créez la classe `PizzaList` dans le fichier `src/pages/PizzaList.js`** :
- cette classe hérite de `Component`
- instanciez-la dans `src/main.js` comme ceci :
	```js
	// `data` est le tableau défini dans `src/data.js`
	const pizzaList = new PizzaList(data);
	document.querySelector( '.pageContent' ).innerHTML = pizzaList.render();
	```
-  pour chaque cellule du tableau `data`, le composant `PizzaList` créera un composant `PizzaThumbnail` associé
-  le code HTML retourné par la méthode `pizzaList.render()` sera une balise `<section class="pizzaList">` dans laquelle sera injectée la combinaison du `render()` de chaque `PizzaThumbnail`

> _**NB :** en théorie, un simple override du constructor et l'utilisation de la méthode [Array.map](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array/map) doivent suffire !_

Le résultat attendu est le suivant :

<img src="images/readme/pizzaland-05.png" >

## D.3. _Propriétés et méthodes statiques :_ La classe Router

Dans cet exercice, je vous propose de développer une classe `Router` qui, à l'aide des **propriétés et méthodes statiques**, va gérer l'affichage à la fois du titre de la page, et de son contenu.

### D.3.1 Rappels de syntaxe
Pour rappel les propriétés et méthodes statiques peuvent se déclarer à l'aide du mot clé `static`. Ces propriétés/méthodes sont définies et s'utilisent au niveau de la classe (et pas de l'instance) comme ceci :

```js
class Counter {
    static counter = 0;
    static getCounter() {
        return this.counter++;
    }
}

console.log(
    Counter.getCounter(),   // 0
    Counter.counter,        // 1
    Counter.getCounter(),   // 1
    Counter.counter,        // 2
);
```

### D.3.2. La classe `Router`
1. **Créez une classe `Router`** (dans un module `src/Router.js`) avec :
	- une propriété statique `titleElement`
	- une propriété statique `contentElement`
	- une propriété statique `routes`
2. **Dans le `src/main.js`, renseignez les valeurs de `titleElement`, `contentElement` et `routes` comme ceci :**

	```js
	Router.titleElement = document.querySelector('.pageTitle');
	Router.contentElement = document.querySelector('.pageContent');
	Router.routes = [
		{path: '/', page: pizzaList, title: 'La carte'}
	];
	```
5. **Enfin développez une méthode statique `Router.navigate(path)` qui permette d'afficher la `PizzaList` et son titre comme ceci :**
	```js
	Router.navigate('/'); // affiche 'La carte' dans .pageTitle
		// et la pizzaList dans .pageContent
	```

## D.4. _Private, Setter & Getter :_ La propriété `pizzaList.pizzas`

Dans cet exercice je vous propose d'utiliser plusieurs syntaxes supplémentaires: les **propriétés et méthodes privées** ainsi que les **getters/setters**.

### D.4.1. Rappels propriétés privées
Pour déclarer et utiliser des propriétés ou méthodes privées il suffit de les préfixer du caractère `'#'`comme ceci :

```js
class Character {
	firstName;
	#canCook = false; // propriété privée (#)
	constructor(firstName) {
		this.firstName = firstName;
		this.#canCook = (firstName === 'Walter');
	}
}
```

Le support des propriétés et méthodes privées est en stage 3/4 de spécification. Ce n'est donc pas encore dans la spec EcmaScript officielle. Néanmoins il est possible de les utiliser grâce à au plugin Babel [@babel/plugin-proposal-class-properties](https://babeljs.io/docs/en/babel-plugin-proposal-class-properties) que l'on a déjà installé précédemment (c'est le même que pour les propriétés publiques).

> _**NB :** Si vous vous demandez pourquoi on écrit `#propriete` et pas `private propriete` comme dans d'autres langages, la réponse se trouve ici :_ https://github.com/tc39/proposal-class-fields/blob/master/PRIVATE_SYNTAX_FAQ.md#why-arent-declarations-private-x

### D.4.2. Rappels getters/setters
Vous pouvez déclarer des getter et des setters de la façon suivante :

```js
class Character {
	#firstName;
	set firstName(value) {
		console.log(value);
		this.#firstName = value.toLowerCase();
	}
}
```

Ce sont en fait des méthodes qui se "déguisent" en propriétés. Pour utiliser le setter écrit au dessus, on peut faire simplement :
```js
heisenberg.firstName = 'Walter';
```
On a l'impression d'utiliser une propriété, mais en réalité c'est une méthode qui est déclenchée, et donc le `console.log(value)` va s'exécuter.

### D.4.3. Mise en oeuvre
A l'aide des propriétés privées, des getters et des setters, faire en sorte que le code suivant permette d'afficher la liste des pizzas :
```js
const pizzaList = new PizzaList([]);
Router.routes = [{ path: '/', page: pizzaList, title: 'La carte' }];

Router.navigate('/'); // affiche une page vide
pizzaList.pizzas = data;
Router.navigate('/'); // affiche la liste des pizzas

```
