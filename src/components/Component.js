export default class Component {
	tagName;
	attribute;
	children;
	constructor(tagName, attribute, children) {
		this.tagName = tagName;
		this.attribute = attribute;
		this.children = children;
	}
	render() {
		let child = '';
		if (this.children) {
			if (this.children instanceof Array) {
				this.children.forEach(element => {
					if (element instanceof Component) child += element.render();
					else child += element;
				});
			} else {
				child += this.children;
			}
			if (this.children && this.attribute) {
				return `<${this.tagName} ${this.attribute.name}=${this.attribute.value} >${child}</${this.tagName}>`;
			}
		} else if (!this.children && this.attribute) {
			return `<${this.tagName} ${this.attribute.name}=${this.attribute.value}/>`;
		}
		return `<${this.tagName}>${child}</${this.tagName}>`;
	}

	set children(value) {
		this.children = value;
	}
}
