import Component from './Component.js';
import Img from './Img.js';

export default class pizzaThumbnail extends Component {
	constructor(data) {
		super('article', { name: 'class', value: 'pizzaThumbnail' }, [
			new Component('a', { name: 'href', value: data.image }, [
				new Img(data.image),
				new Component('section', null, [
					new Component('h4', null, data.name),
					new Component('ul', null, [
						new Component('li', null, `Prix petit format :${data.price_small}`),
						new Component('li', null, `Prix grand format :${data.price_large}`),
					]),
				]),
			]),
		]);
	}
}
