import Component from './components/Component.js';

export default class Router {
	static titleElement;
	static contentElement;
	static routes;

	static navigate(path) {
		let idx = -1;
		let i = -1;
		this.routes.forEach(element => {
			i++;
			if (element.path === path) idx = i;
		});
		if (idx != -1) {
			const title = new Component('h1', null, this.routes[idx].title);
			this.titleElement.innerHTML = title.render();
			this.contentElement.innerHTML = this.routes[idx].page.render();
		}
	}
}
