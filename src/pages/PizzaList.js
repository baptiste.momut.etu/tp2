import Component from '../components/Component.js';
import PizzaThumbnail from '../components/PizzaThumbnail.js';

export default class PizzaList extends Component {
	#pizzas;
	constructor(data) {
		super(
			'section',
			{ name: 'class', value: 'pizzaList' },
			data.map(elt => new PizzaThumbnail(elt))
		);
	}

	set pizzas(value) {
		this.#pizzas = value.map(elt => new PizzaThumbnail(elt));
		super.children = this.#pizzas;
	}
}
