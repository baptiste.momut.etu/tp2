import Component from './components/Component.js';
import Img from './components/Img.js';
import PizzaThumbnail from './components/PizzaThumbnail.js';
import data from './data.js';
import PizzaList from './pages/PizzaList.js';
import Router from './Router.js';

//const pizzaList = new PizzaList(data);
const pizzaList = new PizzaList([]);
const pizzaThumbnail = new PizzaThumbnail(data[0]);

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');

/*Router.routes = [
	{ path: '/carte', page: pizzaList, title: 'La carte' },
	{ path: '/regina', page: pizzaThumbnail, title: 'Regina' },
];*/

//Router.navigate('/carte');

Router.routes = [{ path: '/', page: pizzaList, title: 'La carte' }];

Router.navigate('/'); // affiche une page vide
pizzaList.pizzas = data;
Router.navigate('/'); // affiche la liste des pizzas
